﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Model;
using DAL;

namespace BLL
{
	public class AppHistoryBll
	{
		/// <summary>
		/// 实例化数据访问层的代码
		/// </summary>
		AppHistoryDal dal = new AppHistoryDal();
		/// <summary>
		/// 查询用户历史题
		/// </summary>
		/// <returns></returns>
		public List<Historical> GetHistoryical(int Uid)
		{
			return dal.GetHistoryical(Uid);
		}
		/// <summary>
		/// 修改历史题的状态
		/// </summary>
		/// <returns></returns>
		public Historical HistoryId(int id)
		{
			return dal.HistoryId(id);
		}
		/// <summary>
		/// 将未掌握的题修改为已掌握
		/// </summary>
		/// <param name="history"></param>
		/// <returns></returns>
		public bool EditHistory(Historical history)
		{
			return dal.EditHistory(history);
		}
	}
}
