﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Model;
using DAL;

namespace BLL
{
    public class UserInfoBLL
    {
        UserInfoDAL dal = new UserInfoDAL();
        /// <summary>
        /// 登录
        /// </summary>
        /// <param name="Uname"></param>
        /// <param name="Pwd"></param>
        /// <returns></returns>
        public UserInfo Louser(string Uname, string Pwd)
        {
            return dal.Louser(Uname, Pwd);
        }
        /// <summary>
        /// 注册
        /// </summary>
        /// <param name="u"></param>
        /// <returns></returns>
        public bool AddUser(UserInfo u)
        {
            return dal.AddUser(u);
        }
    }
}
