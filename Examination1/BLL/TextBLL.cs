﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.Entity;
using Model;
using DAL;
namespace BLL
{
   public class TextBLL
    {
       TextDAL dal = new TextDAL();
       /// <summary>
       /// 考试题显示
       /// </summary>
       /// <param name="id"></param>
       /// <returns></returns>
       public List<TextModel> ShowText(string name)
       {
           return dal.ShowText(name);
       }
       /// <summary>
       /// 显示试题试卷，公司名称
       /// </summary>
       /// <returns></returns>
       public Dictionary<string, string> GetExamExcal(int id)
       {
           return dal.GetExamExcal(id);
       }
       /// <summary>
       /// 根据公司id获取名称
       /// </summary>
       /// <param name="id"></param>
       /// <returns></returns>
       public string GetCompanyName(int id)
       {
           return dal.GetCompanyName(id);
       }
       /// <summary>
       /// 随机抽题
       /// </summary>
       /// <param name="id"></param>
       /// <returns></returns>
       public TextModel GetText(int id)
       {
           return dal.GetText(id);
       }
      
       /// <summary>
       /// 添加已掌握
       /// </summary>
       /// <param name="model"></param>
       /// <returns></returns>
       public bool AddHist1(Historical model)
       {
           return dal.AddHist1(model);
       }
       /// <summary>
       /// 添加重要标记
       /// </summary>
       /// <param name="model"></param>
       /// <returns></returns>
       public bool AddHist2(Historical model)
       {
           return dal.AddHist2(model);
       }
       /// <summary>
       /// 根据题号提取题信息
       /// </summary>
       /// <param name="id"></param>
       /// <returns></returns>
       public TextModel GetID(int id)
       {
           return dal.GetID(id);
       }
       /// <summary>
       /// 根据题号查询参考答案
       /// </summary>
       /// <param name="id"></param>
       /// <returns></returns>
       public List<TextModel> GetTid(int id)
       {
           return dal.GetTid(id);
       }
    }
}
