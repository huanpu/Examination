﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.Entity;
using Model;
using DAL;
namespace BLL
{
    public class CompanyBLL
    {
        CompanyDAL dal = new CompanyDAL();
        /// <summary>
        /// 公司显示
        /// </summary>
        /// <param name="index"></param>
        /// <param name="size"></param>
        /// <returns></returns>
        public List<CompanyModel> ShowCompany()
        {

            return dal.ShowCompany();
        }
        /// <summary>
        /// 公司的添加
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public bool AddCompany(CompanyModel model)
        {
            return dal.AddCompany(model);
        }
        /// <summary>
        /// 查询公司总数
        /// </summary>
        /// <returns></returns>
        public int Count()
        {
            return dal.Count();
        }
        /// <summary>
        /// 根据公司名称搜索
        /// </summary>
        /// <param name="index"></param>
        /// <param name="size"></param>
        /// <returns></returns>
        public List<CompanyModel> SreachCompany(string CompanyName)
        {

            return dal.SreachCompany(CompanyName);
        }


    }
}
