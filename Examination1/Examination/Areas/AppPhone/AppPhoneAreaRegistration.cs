﻿using System.Web.Mvc;

namespace Examination.Areas.AppPhone
{
	public class AppPhoneAreaRegistration : AreaRegistration
	{
		public override string AreaName
		{
			get
			{
				return "AppPhone";
			}
		}

		public override void RegisterArea(AreaRegistrationContext context)
		{
			context.MapRoute(
				"AppPhone_default",
				"AppPhone/{controller}/{action}/{id}",
				new { action = "Index", id = UrlParameter.Optional }
			);
		}
	}
}
