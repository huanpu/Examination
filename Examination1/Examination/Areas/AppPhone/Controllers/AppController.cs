﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using BLL;
using Model;
using MvcApplication7;


namespace Examination.Areas.AppPhone.Controllers
{
    public class AppController : Controller
    {
        Function fun = new Function();
        //
        // GET: /AppPhone/App/
        /// 密匙
        /// </summary>
        private static string Key
        {
            get
            {
                return "hongye10";
            }
        }
        UserInfoBLL bll = new UserInfoBLL();
        public ActionResult Index()
        {
            return View();
        }
        [HttpPost]
        public ActionResult Index(string Uname, string Pwd)
        {
            
            Pwd = MD5class.DesEncrypt(Pwd, Key);
            var data = bll.Louser(Uname, Pwd);
            if (data!=null)
            {

                Session["Uid"] =data.Uid;
                return Content("<script>alert('登录成功！');location.href='/AppPhone/App/Index2'</script>");
            }
            else
            {
                return Content("<script>alert('登录失败！');location.href='/AppPhone/App/Index'</script>");
            }
        }
        public ActionResult AddUser()
        {
            return View();
        }
        [HttpPost]
        public ActionResult AddUser(UserInfo u)
        {
            UserInfo uu = new UserInfo();
            uu.Uname = u.Uname;
            uu.Pwd = MD5class.DesEncrypt(u.Pwd, Key);
            if(fun.userinfo.Where(p=>p.Uname==u.Uname).Count()==0)
            {
            if (bll.AddUser(uu))
            {
                return Content("<script>alert('注册成功！');location.href='/AppPhone/App/Index'</script>");
            }
            else
            {
                return Content("<script>alert('注册失败！');location.href='/AppPhone/App/AddUser'</script>");

            }
            }
            else
            {
                return Content("<script>alert('用户名已存在！');location.href='/AppPhone/App/AddUser'</script>");
            }
        }
        public ActionResult Index2()
        {
            return View();
        }
    }
}
