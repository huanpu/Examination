﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using Model;
using BLL;

namespace Examination.Areas.AppPhone.Controllers
{
    public class AppSheetController : Controller
    {
        //
        // GET: /AppPhone/AppSheet/
        TextBLL bll = new TextBLL();
        int dd;
        public ActionResult Index()
        {
            int id =Convert.ToInt32(Session["Uid"]);
            TextModel model = bll.GetText(id);
            return View(model);
        }
        /// <summary>
        /// 随机题
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public ActionResult RandomText()
        {
            int id = Convert.ToInt32(Session["Uid"]);
            TextModel model = bll.GetText(id);
            return View("Index",model);
        }
        /// <summary>
        /// 添加已掌握
        /// </summary>
        /// <param name="Tid"></param>
        /// <returns></returns>
        public ActionResult AddHistory1(int id)
        {
            TextModel model = bll.GetID(id);
            Historical hist = new Historical();
            hist.Datime = DateTime.Now.ToString();
            hist.Uid = (int)Session["Uid"];
            hist.Tid = model.Tid;
            hist.Ename = model.TName;
            hist.Estate = 1;
            hist.Eresult = model.TAnswer;
            if (bll.AddHist1(hist))
            {
                return Json(1, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json(0, JsonRequestBehavior.AllowGet);
            }
        }
        /// <summary>
        /// 添加重要标记
        /// </summary>
        /// <param name="Tid"></param>
        /// <returns></returns>
        public ActionResult AddHistory2(int id)
        {
            TextModel model = bll.GetID(id);
            Historical hist = new Historical();
            hist.Datime = DateTime.Now.ToString();
            hist.Uid = (int)Session["Uid"];
            hist.Tid = model.Tid;
            hist.Ename = model.TName;
            hist.Estate = 2;
            hist.Eresult = model.TAnswer;
            if (bll.AddHist1(hist))
            {
                return Json(1, JsonRequestBehavior.AllowGet);
            }
            else
            {
                return Json(0, JsonRequestBehavior.AllowGet);
            }
        }
    }
}
