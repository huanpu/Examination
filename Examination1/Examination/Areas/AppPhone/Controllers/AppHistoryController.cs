﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using BLL;
using Model;

namespace Examination.Areas.AppPhone.Controllers
{
    public class AppHistoryController : Controller
    {
        //
        // GET: /AppPhone/AppHistory/
		//实例化逻辑访问层的代码
		AppHistoryBll bll = new AppHistoryBll();
        public ActionResult Index(int Index = 1, int Size = 2)
        {
            int Uid = (int)Session["Uid"];
            //查询重点标记的题目
            int Count = bll.GetHistoryical(Uid).OrderByDescending(p => p.Hid).Where(p => p.Estate == 2).Count();
            int PageCount = Count / Size;
            PageCount = Count % Size == 0 ? Count / Size : Count / Size + 1;
            var data = bll.GetHistoryical(Uid).OrderByDescending(p => p.Hid).Where(p => p.Estate == 2).Skip((Index - 1) * Size).Take(Size).ToList();
            ViewBag.se = "Uid=" + Session["Uid"] + "&Estate=" + 2; //显示保留数据
            ViewBag.Index = Index;
            ViewBag.Size = Size;
            ViewBag.PageCount = PageCount;
            ViewBag.SizeCount = Count;
            ViewBag.significance = data;
            return View();

        }
		/// <summary>
		/// 显示历史题
		/// </summary>
		/// <returns></returns>
		public ActionResult GetHistory(int Index = 1, int Size = 2)
		{
            int Uid = (int)Session["Uid"];
			//查询重点标记的题目
			int count = bll.GetHistoryical(Uid).OrderByDescending(p => p.Hid).Where(p => p.Estate == 1).Count();
			int pagecount = count / Size;
			pagecount = count % Size == 0 ? count / Size : count / Size + 1;
			var da = bll.GetHistoryical(Uid).OrderByDescending(p => p.Hid).Where(p => p.Estate == 1).Skip((Index-1)*Size).Take(Size).ToList();
			ViewBag.s = "Uid=" + Session["Uid"] + "&Estate=" + 1; //显示保留数据
			ViewBag.index = Index;
			ViewBag.size = Size;
			ViewBag.pagecount = pagecount;
			ViewBag.sizecount = count;
			//查询已掌握的题目
            return View(da);

		}
        /// <summary>
        /// 我的全部试题
        /// </summary>
        /// <param name="Index"></param>
        /// <param name="Size"></param>
        /// <returns></returns>
        public ActionResult GetAll(int Index = 1, int Size = 2)
        {
            int Uid = (int)Session["Uid"];
            //查询重点标记的题目
            int Count = bll.GetHistoryical(Uid).OrderByDescending(p => p.Hid).Count();
            int PageCount = Count / Size;
            PageCount = Count % Size == 0 ? Count / Size : Count / Size + 1;
            var data = bll.GetHistoryical(Uid).OrderByDescending(p => p.Hid).Skip((Index - 1) * Size).Take(Size).ToList();
            ViewBag.se = "Uid=" + Session["Uid"]; //显示保留数据
            ViewBag.Index = Index;
            ViewBag.Size = Size;
            ViewBag.PageCount = PageCount;
            ViewBag.SizeCount = Count;
            ViewBag.significance = data;
            return View();

        }
		/// <summary>
		/// 修改历史题的状态
		/// </summary>
		/// <param name="id"></param>
		/// <returns></returns>
        public ActionResult EditHistory(int id)
		{
			var data = bll.HistoryId(id);
            if (data.Estate == 1 )
			{
				data.Estate = 2;
				bll.EditHistory(data);
				return Content("<script>alert('标记成功');location.href='/AppPhone/AppHistory/GetAll'</script>");
			}
            else if (data.Estate == 2 )
			{
				data.Estate = 1;
				bll.EditHistory(data);
                return Content("<script>alert('已进入掌握列表');location.href='/AppPhone/AppHistory/GetAll'</script>");
            }
            else
                if (data.Estate == 3 )
            {
                data.Estate = 2;
                bll.EditHistory(data);
                return Content("<script>alert('标记成功');location.href='/AppPhone/AppHistory/GetAll'</script>");
            }
			else
			{
				return View();
			}
		}

    }
}
