﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using System.Data;
using System.Data.Entity;
using Model;
using BLL;
using Webdiyer.WebControls.Mvc;
using System.IO;
namespace Examination.Controllers
{
    public class HomeController : Controller
    {
        //
        // GET: /Home/

        /// <summary>
        /// 实例化
        /// </summary>
        CompanyBLL companybll = new CompanyBLL();//公司
        TextBLL textbll = new TextBLL();//试题

        /// <summary>
        /// 显示公司
        /// </summary>
        /// <param name="index"></param>
        /// <returns></returns>
        public ActionResult Index(int index = 1)
        {
            int size = 8;
            PagedList<CompanyModel> list = companybll.ShowCompany().ToPagedList(index, size);
            return View(list);
        }
       
        /// <summary>
        /// 公司添加
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public ActionResult AddCompany()
        {
            return PartialView();
        }
        [HttpPost]
		public ActionResult AddCompany(CompanyModel model, HttpPostedFileBase file)
        {
			if (file != null && file.ContentLength > 0)
			{
				
				//保存上传文件的路径
				var fileName = Path.Combine(Request.MapPath("~/UpLoadFile"), Path.GetFileName(file.FileName));
				file.SaveAs(fileName);
				model.Companylog = "/UpLoadFile/" + Path.GetFileName(file.FileName);
				if (companybll.AddCompany(model))
				{
					return Content("<script>alert('添加成功');location.href='/Home/AddCompany'</script>");
				}
				else
				{
					return Content("<script>alert('添加失败');location.href='/Home/AddCompany'</script>");
				}
			}
			else
			{
				//Request.Files.Count 文件数为0上传不成功
				return Content("<script>alert('请选择要上传的LOGO ！！！');location.href='/Home/AddCompany'</script>");
			}
           
        }
    }
}
