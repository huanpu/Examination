﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Model
{
    /// <summary>
    /// 公司表
    /// </summary>
    public class CompanyModel
    {
        /// <summary>
        /// 公司编号
        /// </summary>
         [Key]        
        public int CompanyId { get; set; }
        /// <summary>
        /// 公司名称
        /// </summary>
        public string CompanyName { get; set; }
        /// <summary>
        /// 公司图片
        /// </summary>
        public string Companylog { get; set; }
        /// <summary>
        /// 公司介绍  
        /// </summary>
        public string CompanyRemark { get; set; }
        /// <summary>
        /// 公司实力 
        /// </summary>
        public string CompanyImg { get; set; }
        /// <summary>
        /// 公司简介2
        /// </summary>
        public string CompanyRemark2 { get; set; }
    }
}
