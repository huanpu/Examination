﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations.Schema;
namespace Model
{
   public class TextModel
    {
       [Key]
       public int Tid { get; set; }
       public string TName { get; set; }
       public string TAnswer { get; set; }
       [ForeignKey("companyModel")]
       public int CompanyId { get; set; }
	   public string ExcelName { get; set; }
       public CompanyModel companyModel { get; set; }

    }
}
//试卷表Text
//TID   主键
//TName    面试题  string
//TAnswer    面试题答案   string
//CID 		公司外键