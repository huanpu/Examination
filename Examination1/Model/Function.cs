﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data;
using System.Data.Entity;

namespace Model
{
    /// <summary>
    /// 数据上下文
    /// </summary>
   public class Function:DbContext
    {
     
       /// <summary>
       /// 公司
       /// </summary>
       public DbSet<CompanyModel> CompanyModel { get; set; }
       /// <summary>
       /// 试题
       /// </summary>
       public DbSet<TextModel> TextModel { get; set; }
       /// <summary>
       /// 用户
       /// </summary>
       public DbSet<UserInfo> userinfo { get; set; }
       /// <summary>
       /// 历史
       /// </summary>
       public DbSet<Historical> historical { get; set; }
    }
}
