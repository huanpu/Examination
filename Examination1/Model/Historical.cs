﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;

namespace Model
{
    /// <summary>
    /// 历史记录
    /// </summary>
    public class Historical
    {
        [Key]
        public int Hid { get; set; }//编号
        public int Tid { get; set; }//题号
        public string Ename { get; set; }//题目
        public string Eresult { get; set; }//答案
        public string Datime { get; set; }//答题时间
        public int Estate { get; set; }//状态（1：已掌握  2： 重要标记）
        [ForeignKey("userinfo")]
        public int Uid { get; set; }//用户ID
        public virtual UserInfo userinfo { get; set; }
    }
}
