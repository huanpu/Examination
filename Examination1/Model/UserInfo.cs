﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.ComponentModel.DataAnnotations;

namespace Model
{
    /// <summary>
    /// 用户表
    /// </summary>
    public class UserInfo
    {
        [Key]
        public int Uid { get; set; }//用户ID
        public string Uname { get; set; }//用户名
        public string Pwd { get; set; }//密码
        public string Email { get; set; }//邮箱
    }
}
