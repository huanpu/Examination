﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Model;
using System.Data;

namespace DAL
{
	public class AppHistoryDal
	{
		Function fun = new Function();//实例化数据上下文类
		/// <summary>
		/// 查询用户历史题
		/// </summary>
		/// <returns></returns>
		public List<Historical> GetHistoryical(int Uid)
		{
			return fun.historical.Where(p => p.Uid == Uid).ToList();
		}
		/// <summary>
		/// 修改历史题的状态
		/// </summary>
		/// <returns></returns>
		public Historical HistoryId(int id)
		{
			return fun.historical.Find(id);
		}
		/// <summary>
		/// 将未掌握的题修改为已掌握
		/// </summary>
		/// <param name="history"></param>
		/// <returns></returns>
		public bool EditHistory(Historical history)
		{
			fun.Entry<Historical>(history).State = EntityState.Modified;
			return fun.SaveChanges() > 0;
		}
	}
}
