﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;
using Model;
using System.Data;
namespace DAL
{
   public  class TextDAL
    {
       Function fun = new Function();
       /// <summary>
       /// 考试题显示
       /// </summary>
       /// <param name="id"></param>
       /// <returns></returns>
       public List<TextModel> ShowText(string name)
       {
           List<TextModel> list = fun.TextModel.Where(p => p.ExcelName == name).ToList();
           return list;
       }
       /// <summary>
       /// 显示试题试卷，公司名称
       /// </summary>
       /// <returns></returns>
       public Dictionary<string, string> GetExamExcal(int id)
       {
           Dictionary<string, string> exam = new Dictionary<string, string>();
           if (id == 0)
           {
               var m1 = fun.TextModel.GroupBy(x => x.ExcelName).Select(x => new { Group = x.Key, Menber = x });
               exam.Clear();
               foreach (var g in m1)
               {
                   foreach (var m in g.Menber.Take(1))
                   {
                       exam.Add(m.ExcelName, m.CompanyId.ToString());
                   }
               }
               return exam;
           }
           else
           {
               var m1 = fun.TextModel.Where(x => x.CompanyId == id).GroupBy(x => x.ExcelName).Select(x => new { Group = x.Key, Menber = x });
               exam.Clear();
               foreach (var g in m1)
               {
                   foreach (var m in g.Menber.Take(1))
                   {
                       exam.Add(m.ExcelName, m.CompanyId.ToString());
                   }
               }
               return exam;
           }
       }
       /// <summary>
       /// 根据公司id获取名称
       /// </summary>
       /// <param name="id"></param>
       /// <returns></returns>
       public string GetCompanyName(int id)
       {
           return fun.CompanyModel.Where(x => x.CompanyId == id).FirstOrDefault().CompanyName;
       }
       /// <summary>
       /// 随机抽题
       /// </summary>
       /// <param name="id"></param>
       /// <returns></returns>
       public TextModel GetText(int id)
       {
           int dd=0;
           for (int i = 1; i > 0; i++)
           {
               Random ran = new Random();
               int RandKey = ran.Next(1, fun.TextModel.Count());
               if (fun.historical.Where(p =>p.Uid==id && p.Tid == RandKey && p.Estate == 1).Count() == 0)
               {
                   dd = RandKey;
                   break;
               }

           }
           TextModel model = fun.TextModel.Where(p => p.Tid == dd).FirstOrDefault();
           //添加历史记录
           Historical mm = new Historical();
           mm.Uid = id;
           mm.Datime = DateTime.Now.ToString();
           mm.Ename = model.TName;
           mm.Eresult = model.TAnswer;
           mm.Estate = 3;
           mm.Tid = model.Tid;
           fun.historical.Add(mm);
           fun.SaveChanges();
           return model;
       }
      
       /// <summary>
       /// 添加已掌握
       /// </summary>
       /// <param name="model"></param>
       /// <returns></returns>
       public bool AddHist1(Historical model)
       {
           fun.historical.Add(model);
           return fun.SaveChanges() > 0;
       }
       /// <summary>
       /// 添加重要标记
       /// </summary>
       /// <param name="model"></param>
       /// <returns></returns>
       public bool AddHist2(Historical model)
       {
           fun.historical.Add(model);
           return fun.SaveChanges() > 0;
       }
       /// <summary>
       /// 根据题号提取题信息
       /// </summary>
       /// <param name="id"></param>
       /// <returns></returns>
      public TextModel GetID(int id)
       {
           TextModel model= fun.TextModel.Where(p => p.Tid == id).FirstOrDefault();
           return model;
       }

      /// <summary>
      /// 根据题号查询参考答案
      /// </summary>
      /// <param name="id"></param>
      /// <returns></returns>
      public List<TextModel> GetTid(int id)
      {
          List<TextModel> list = fun.TextModel.Where(p => p.Tid == id).ToList();
          return list;
      }
    }
}
