﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Data.Entity;
using Model;
using System.Data;
namespace DAL
{
    public class CompanyDAL
    {
        Function fun = new Function();
        /// <summary>
        /// 公司显示
        /// </summary>
        /// <param name="index"></param>
        /// <param name="size"></param>
        /// <returns></returns>
        public List<CompanyModel> ShowCompany()
        {

            List<CompanyModel> list = fun.CompanyModel.ToList();
            return list;
        }
        /// <summary>
        /// 公司的添加
        /// </summary>
        /// <param name="model"></param>
        /// <returns></returns>
        public bool AddCompany(CompanyModel model)
        {
            fun.CompanyModel.Add(model);
            return fun.SaveChanges() > 0;
        }
        /// <summary>
        /// 查询公司总数
        /// </summary>
        /// <returns></returns>
        public int Count()
        {
            int count = fun.CompanyModel.Count();
            return count;
        }
        /// <summary>
        /// 根据公司名称搜索
        /// </summary>
        /// <param name="index"></param>
        /// <param name="size"></param>
        /// <returns></returns>
        public List<CompanyModel> SreachCompany(string CompanyName)
        {

			List<CompanyModel> list = fun.CompanyModel.Where(p => (string.IsNullOrEmpty(CompanyName)) ? true : p.CompanyName.Contains(CompanyName)).ToList();
            return list;
        }
    
      
    }
}
