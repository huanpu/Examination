﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Model;

namespace DAL
{
    public class UserInfoDAL
    {
        Function fun = new Function();
        /// <summary>
        /// 登录
        /// </summary>
        /// <param name="Uname"></param>
        /// <param name="Pwd"></param>
        /// <returns></returns>
        public UserInfo Louser(string Uname, string Pwd)
        {
            var data = fun.userinfo.Where(p => p.Uname == Uname && p.Pwd == Pwd).FirstOrDefault();
            return data;
        }
        /// <summary>
        /// 注册
        /// </summary>
        /// <param name="u"></param>
        /// <returns></returns>
        public bool AddUser(UserInfo u)
        {
            fun.userinfo.Add(u);
            return fun.SaveChanges() > 0;
        }
        
    }
}
